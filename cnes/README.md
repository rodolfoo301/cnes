# Cadastro Nacional de Estabelecimentos de Saúde (CNES) no Brasil.

## Tecnologias utilizadas
- Spring boot
- Java 8

## Banco utilizado
- PostgreSQL

## Configuração do Banco
	- usuário: root
	- senha: root
	- Criar Banco de dados "cnes"
	
## Configurar aplicação
	- Baixar as dependências do Maven;
	- Apontar para JDK;
	- Executar o "run as > java Application" na classe "CnesApplication";
	- Após startar, acessar: http://localhost:8080/swagger-ui.html
	
## Popular banco de dados
	- No diretório do projeto, tem um arquivo json, que é convertido para uma DTO, após isso faz  o mapper para a entidade e faz a persistência de 	cada ítem; 
	- Executar o popula banco para salvar todos os intens do arquivo json;
	- Após isso, realizar consultas. Filtrar por Tipo de CNES, Estado e o número da pagina (50 itens por página, parâmetro obrigatório);
	
	
## 