package br.com.rodolfo.cnes.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.rodolfo.cnes.configuration.entities.Cnes;

@Repository
public interface CnesRespository extends CrudRepository<Cnes, Long>{

}
