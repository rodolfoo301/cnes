package br.com.rodolfo.cnes.configuration;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

@Configuration
public class BDConfiguration {
	
	private static final String DIALECT_POSTGRES = "org.hibernate.dialect.PostgreSQLDialect";
	private static final String PSW_POSTGRES = "root";
	private static final String USER_POSTGRES = "root";
	private static final String JDBC_POSTGRES = "jdbc:postgresql://localhost:5432/cnes";
	private static final String DRIVER_POSTGRES = "org.postgresql.Driver";

	@Bean
    public DataSource dataSource(){
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(DRIVER_POSTGRES);
        dataSource.setUrl(JDBC_POSTGRES);
        dataSource.setUsername(USER_POSTGRES);
        dataSource.setPassword(PSW_POSTGRES);
        return dataSource;
    }
	
	@Bean
	public JpaVendorAdapter jpaVendorAdapter(){
		HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
		adapter.setDatabase(Database.POSTGRESQL);
		adapter.setShowSql(true);
		adapter.setGenerateDdl(true);
		adapter.setDatabasePlatform(DIALECT_POSTGRES);
		adapter.setPrepareConnection(true);
		return adapter;
	}

}
