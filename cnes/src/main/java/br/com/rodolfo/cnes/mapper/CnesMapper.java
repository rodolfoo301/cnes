package br.com.rodolfo.cnes.mapper;

import br.com.rodolfo.cnes.configuration.entities.Cnes;
import br.com.rodolfo.cnes.dto.CnesDTO;

public class CnesMapper {
	
	private CnesMapper() {
		
	}

	public static Cnes mapper(CnesDTO cnesDTO) {
		Cnes cnes = new Cnes();
		cnes.setCodigoCnes(Long.parseLong(cnesDTO.getCo_cnes()));
		cnes.setCodigoIbge(Long.parseLong(cnesDTO.getCo_ibge()));
		cnes.setNomeFantasia(cnesDTO.getNo_fantasia());
		cnes.setTipoUnidade(cnesDTO.getDs_tipo_unidade());
		cnes.setTipoGestao(cnesDTO.getTp_gestao());
		cnes.setNomeLogradouro(cnesDTO.getNo_logradouro());
		cnes.setNumeroEndereco(cnesDTO.getNu_endereco());
		cnes.setNomeBairro(cnesDTO.getNo_bairro());
		cnes.setCep(cnesDTO.getCo_cep());
		cnes.setUf(cnesDTO.getUf());
		cnes.setMunicipio(cnesDTO.getMunicipio());
		cnes.setTelefone(cnesDTO.getNu_telefone());
		
		return cnes;
	}

}
