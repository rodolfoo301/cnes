package br.com.rodolfo.cnes.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import br.com.rodolfo.cnes.configuration.entities.Cnes;

@Repository
public class CnesDAO {

	private static final int TOTAL_REGISTRO_POR_PAGINA = 50;
	private static final String UF = "uf";
	private static final String NOME_FANTASIA = "nomeFantasia";
	private static final String OPERADOR_LIKE = "%";

	@PersistenceContext
	private EntityManager em;

	public List<Cnes> consultarCnes(String tipo, String estado, Integer numeroPagina) {
		CriteriaBuilder builder = em.getCriteriaBuilder();
		CriteriaQuery<Cnes> criteria = builder.createQuery(Cnes.class);
		Root<Cnes> fromCnes = criteria.from(Cnes.class);
		List<Predicate> parametros = new ArrayList<>();

		if (tipo != null && !tipo.isEmpty()) {
			parametros.add(builder.like(builder.upper(fromCnes.get(NOME_FANTASIA)),
					OPERADOR_LIKE + tipo.toUpperCase() + OPERADOR_LIKE));
		}
		if (estado != null && !estado.isEmpty()) {
			parametros.add(builder.like(builder.upper(fromCnes.get(UF)),
					OPERADOR_LIKE + estado.toUpperCase() + OPERADOR_LIKE));
		}

		criteria.select(fromCnes).where(parametros.toArray(new Predicate[parametros.size()]));

		return em.createQuery(criteria).setFirstResult(calcularPaginacaoAtual(numeroPagina, TOTAL_REGISTRO_POR_PAGINA))
				.setMaxResults(TOTAL_REGISTRO_POR_PAGINA).getResultList();
	}

	public static Integer calcularPaginacaoAtual(Integer numeroPagina, Integer numeroMaximoRegistros) {
		final Integer numeroAuxiliar = 1;
		return (numeroPagina - numeroAuxiliar) * numeroMaximoRegistros;
	}

}
